var contents  = {

	avatar : "img/propic.jpg",

	name : "Lalnuntluanga",

	designation : "Web UI Developer",

	mobile : "+919986510839",

	email : "tluanga34@gmail.com",

	address : {
		text : "#72, Kasavanahalli, Amrutha college Road (Central Jail road), Carmelanram Post, Bangalore - 560035",
		mapUrl : "https://www.google.co.in/maps/place/Wama+Ibbanee,+76,+Valliyamma+Layout+Cross+Rd,+Valliyamma+Layout,+Kasavanahalli,+Bengaluru,+Karnataka+560035/@12.9098658,77.6743645,17z/data=!4m2!3m1!1s0x3bae136cb0222331:0x55a385ecb4cbb893?hl=en"
	},

	socialAccounts : [
		{
			name : "Gitub",
			icon : "img/github.png",
			url : "https://www.github.com/tluanga34",
			id : "tluanga34"
		},
		{
			name : "LinkedIn",
			icon : "img/linkedln.png",
			url : "https://www.linkedin.com/in/lalnuntluanga-chhakchhuak-04aa7463/",
			id : "lalnuntluanga-chhakchhuak-04aa7463"
		},
	],

	careerStartDate : new Date("09/06/2013"),

	introduction : "After being raised and educated from Mizoram, I moved to Bengaluru, Karnataka in search for a job. I landed to Jivox Software as a Creative Consultant Fresher Trainee. I later moved to QA and then a Developer. I'm currently working heavily on Single Page applications where all UI elements and page navigation are handled from in the client side, so that server is used only for requesting dynamic JSON Data and also managing session resulting in more efficient use of servers. <br><br> I also use lots of open-source UI components like a live chart, date picker, pop-up modal etc. Utilizing opensource framework and libraries result in faster development and less error instead of creating every component from the scratch. <br><br> I'm also familiar to front-end build tools like npm, bower, grunt etc. which help to package the UI files in a compact way resulting in smaller file sizes benefiting both user and the server. <br><br> Though I don't have experience in the Back-end side, having a basic understanding of how thing on the server side works is a bit of advantage to me. One of my non-client project called Tagrocket is using a PHP code written by me which takes HTML tags as an input and using POST request and render the tags in the page, and also create a file with that input known as preview link, which is very useful to preview Ad Tags. I am familiar with a LAMP stack. I also have a bit of knowledge on MEAN stack.",

	skills : [
		{
			name : "HTML5",
			level : 8,
		},
		{
			name : "CSS3",
			level : 7,
		},
		{
			name : "Java Script",
			level : 6,
		},
		{
			name : "JQuerry",
			level : 5,
		},
		{
			name : "Angular JS",
			level : 7,
		},
		{
			name : "Git",
			level : 5
		},
		{
			name : "Grunt",
			level : 5
		},
		{
			name : "ionic v1",
			level : 6,
		},
		{
			name : "Cordova",
			level : 6,
		}
	],

	experiences : [
		{
			duration : {
				from : (new Date("08/17/2017").setHours(0,0,0,0)),
				to	: (new Date().setHours(0,0,0,0)),
			},
			title : "Front-End / Hybrid App Developer",
			company : "Presto Apps",
			description : "We're into web app and mobile app developement. We use Ionic and Cordova to build mobile app. We're providing e-commerce solution for small business.",
			website : "http://www.presto-apps.com"
		},
		{
			duration : {
				from : new Date("04/15/2016").setHours(0,0,0,0),
				to	: new Date("08/14/2017").setHours(0,0,0,0),
			},
			title : "Front-End Developer",
			company : "BI Worldwide India",
			description : "Creating Micro Websites, E-commerce web pages and Emailers for internal marketing needs are my main roles.",
			website : "http://www.biworldwide.co.in/"
		},
		{
			duration : {
				from : (new Date("09/06/2013").setHours(0,0,0,0)),
				to	: (new Date("04/15/2016").setHours(0,0,0,0)),
			},
			title : "UI Developer",
			company : "Jivox Software Private Limited",
			description : "My role was to develop a highly functional Custom online display advertisement. A certain types of functionalities such as Parallax, Cover, pop-ups, and controlling HTML5 videos are implemented within the Ads. It also involves animations using JQuery and VelocityJS. I was also working on dynamic and highly personalized Ads which required me to work closely with backend, receiving personalized data based on the user in JSON format and display them in the HTML.",
			website : "https://www.jivox.com/"
		}
		
		
	],
	educations : [		
		{
			course : "Bachelor of Computer Application BCA",
			institution: "Govt Zirtiri Residential Science College",
			duration : {
				from : new Date("01/01/2010").setHours(0,0,0,0),
				to : new Date("01/01/2012").setHours(0,0,0,0)
			},
			affiliate : "Mizoram University",
			website: "http://www.gzrsc.edu.in/",
			grade : "70.43%"
		},

		{
			course : "High School and Higher Education",
			institution: "Helen Lowry Higher Secondary School",
			duration : {
				from : new Date("01/01/2004").setHours(0,0,0,0),
				to : new Date("01/01/2009").setHours(0,0,0,0)
			},
			affiliate : "Mizoram Board of School Education",
			website: "http://www.adventistyearbook.org/default.aspx?page=ViewEntity&EntityID=30374",
			grade : "65.6%"
		},

		{
			course : "Primary and Middle School Education",
			institution: "Pine Hill Adventist Academy",
			duration : {
				from : new Date("01/01/1994").setHours(0,0,0,0),
				to : new Date("01/01/2003").setHours(0,0,0,0)
			},
			affiliate : "Mizoram Board of School Education",
			website: "http://www.indiastudychannel.com/schools/40460-Pine-Hill-Adventist-Academy-School-Champhai.aspx",
			grade : "55%"
		}

	],
	hobbies : [
		{
			title : "Playing Guitar",
			description : "I used to play with my local band but now years had passed by since I left my home town. I really missed those friends and I wish I could have a playing chance agian with my friends",
		},
		{
			title : "Playing Table Tenis",
			description : "Table tenis is a kind of sport which is very suitable for an office workers. It requires less space, less energy, and is very skill intensive as well. Now it is my favourite game"
		},
		{
			title : "Watching Football",
			description : "I am a follower of European Football and my favourite team is Real Madrid. I never miss to watch the game unless the time is in clash with an important schedule"
		},
		{
			title : "Watching Action Movie",
			description : "I'm a huge fan of Kungfu right from my child hood. My Current favourite movies are a super hero movies like Avengers and their Individual movies as well "
		},

	],
	projects : [
		{
			name : "CleanerBins",
			url : "http://staging-websites.s3-website-ap-southeast-1.amazonaws.com/cleanerbins/",
			technologies : ["HTML5","CSS3","JavaScript","JQuery","Angular JS"],
			description : "CleanerBins is a local bin cleaning service in UK. The front end side help uses to signup, register their bins and pay for the cleaning services. We provided them a mobile app, website, and backend admin console for their operations team"
		},
		{
			name : "Freshchops",
			url : "http://staging-websites.s3-website-ap-southeast-1.amazonaws.com/freshchops_common_build",
			technologies : ["HTML5","CSS3","JavaScript","JQuery","Angular JS"],
			description : "Freshchops is a hyderabad based ecommerce merchant who offered a fresh mostly pre-cooked food items to users. They are using our ecommerce solution where we provided them a mobile app, website, and backend admin console for their operations team"
		},
		{
			name : "Ooredoo",
			url : "http://staging-websites.s3-website-ap-southeast-1.amazonaws.com/ooreedoo-wip",
			technologies : ["HTML5","CSS3","JavaScript","JQuery","Angular JS"],
			description : "Ooredoo is a maldives based mobile network operator. We provided them a mobile app and a website."
		},
		{
			name : "Indulge",
			url : "https://goo.gl/c1JhxO",
			technologies : ["HTML5","CSS3","JavaScript","JQuery","Angular JS", "MLab API"],
			description : "This is a static page which showcases our new catalogue which focused on experience rewards in a beautiful and elegant way. I have also integrated the input form with mongo Lab (mLab) and use their API to capture the form input from users using Ajax call."
		},
		{
			name : "Tag Rocket",
			url : "http://qa64.jivox.com/tags/tagrocket/",
			technologies : ["HTML", "CSS", "Vanilla JavaScript", "PHP"],
			description : "This is a small project that I have done while I was in Jivox Software. It is neither a company nor clients’ project but I created this to help myself performing my daily task easier. After a few months it got viral within our company and been used by everyone who need it. It is a great tool that has been used to test an Ad tag or any other HTML tag. It works mostly like JS Fiddle. Ad tags are like a plugin call, where the ad renders where ever we call the ad script tag. It can emulate a certain conditions like where the Ad might be placed. Detail features are written in the manual page – Hyperlink placed at the top right corner."
		},
		{
			name : "AMDOCS CLUB",
			url : "http://amdocsclub.com/",
			technologies : ["HTML", "CSS", "Vanilla JavaScript", "Velocity JS"],
			description : "This is also a project for our client. The main difference is the catalogues are more experience rewards oriented rather than just a product."
		},
		{
			name : "PENNA CEMENT",
			url : "http://rewards.pennacement.in/",
			technologies : ["HTML", "CSS", "Angular JavaScript",],
			description : "This project is a portal for our client Penna Cement. It’s a portal where employees can redeem their points and order an item from the rich set of product catalogue.."
		},
	
	],

	theme : {
		leftPannel : {
			bg : "#fff",
			font : "#0c9fb5"
		} 
	}

}