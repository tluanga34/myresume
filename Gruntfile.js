module.exports = function (grunt) {

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-aws-s3');


  grunt.initConfig({
    copy: {
      main: {
        files: [{
            expand: true,
            flatten: true,
            src: "css/*",
            dest: "dist/css/"
          },
          {
            expand: true,
            flatten: true,
            src: "index.html",
            dest: "dist/"
          },
          {
            expand: true,
            flatten: true,
            src: "contents.js",
            dest: "dist/"
          },
          {
            expand: true,
            flatten: true,
            src: "js/*",
            dest: "dist/js/"
          },
          {
            expand: true,
            flatten: true,
            src: "libs/*",
            dest: "dist/libs/"
          },
          {
            expand: true,
            flatten: true,
            src: "img/*",
            dest: "dist/img/"
          },
          {
            expand: true,
            flatten: true,
            src: "fonts/*",
            dest: "dist/fonts/"
          },
        ]
      }
    },
    aws_s3: {
      all: {
        options: {
          "accessKeyId": "AKIAJEYYWL5EKT4G23PA",
          "secretAccessKey": "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",
          "bucket": "lalnun",
          "access": "public-read",
          "region": "ap-southeast-1",
          // "debug":true,
          "differential": false,
          "params": {
            "CacheControl": "max-age:0"
          }
        },
        files: [{
          expand: true,
          cwd: "dist",
          action: 'upload',
          src: [
            "*",
            "img/**",
            "css/**",
            "fonts/**",
            "js/**",
            "libs/**",
          ],
          dest: "resume"
        }]
      },
    }
  });

  grunt.registerTask("default", ["copy"]);
  grunt.registerTask("staging", ["copy","aws_s3"]);

}