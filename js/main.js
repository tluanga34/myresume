(function(){
	
	var resume = {
		//Calculate the year of experience
		experience : function(){
			var cStart = new Date("09/06/2013"),
				exp = document.getElementById("experience"),
				date = new Date(),
				year,
				month;
			
			year = date.getFullYear() - cStart.getFullYear();					
			month = (date.getMonth()) + (12 - cStart.getMonth());					
			year = (month >= 12)? year : year - 1;
			month = (month >= 12)? (month-12) : month;					
			exp.innerHTML = year + "."+month;
		},
		
		navigation : function(){
			var nav = [
				{
					btn : document.getElementById("exp_btn"),
					anchor : document.getElementById("exp_anch")
				},
				{
					btn : document.getElementById("edu_btn"),
					anchor : document.getElementById("edu_anch")
				},
				{
					btn : document.getElementById("hob_btn"),
					anchor : document.getElementById("hob_anch")
				},
				{
					btn : document.getElementById("proj_btn"),
					anchor : document.getElementById("proj_anch")
				}
			];
								
			for(var i = 0; i < nav.length; i++) {
				(function(indx){
					nav[indx].btn.addEventListener("click",function(e){
						topOffset = nav[indx].anchor.offsetTop - 100;
						Velocity(document.body, "scroll",{offset : topOffset, duration : 1000});
					});
				})(i);
			}
		},
		
		ratings : function(){
			var container = document.getElementsByClassName("ratings"),
				level,
				child;
			//console.log(container);
			
			container.forEach(function(key,indx){
				level = key.getAttribute("data-level");
				child = document.createElement("div");
				child.style.width = (level * 10) + "%";
				key.appendChild(child);
				
				console.log(level);
			});
		},
		
		//Initiate the program
		init : function(){
		
			//PROTOTYPE COPY
			HTMLCollection.prototype.forEach = Array.prototype.forEach;
			NodeList.prototype.forEach = Array.prototype.forEach;
			
			//START EACH PART OF THE PROGRAM
			this.experience();
			this.navigation();
			//this.ratings();
		}
	};
	//Initiate call
	resume.init();
})();


angular.module("app",[])

.controller("Common",["$scope",function(scope){

	scope.contents = contents;

	scope.today = (new Date().setHours(0,0,0,0))

	console.log(scope.today);


}])


.directive("htmlBind", ["$compile", "$parse", function ($compile, $parse) {

	
	return {
		scope : true,
		link: function (scope, elem, attr) {

			

			attr.$observe('htmlBind', function (newValue, oldValue) {
				var value = scope.$parent.$eval(attr.htmlBind);

				console.log("Change");
				console.log(value);
				elem.html(value);
				$compile(elem.contents())(scope);
			});
		}
	}


}])